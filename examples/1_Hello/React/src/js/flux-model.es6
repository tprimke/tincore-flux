var flux = require('tc-flux');

// initialise the module
var init = function() {
  // reset the Flux
  flux.clear();

  // create the store
  flux.createStore({
    name: 'Name',
    init: '',
    data: 'name',
    onData: (callbacks, data) => {
      callbacks.setValue(data);
      callbacks.updated();
    }
  });

  // create the action
  flux.createAction({
    name: 'set-name',
    onRun: (callbacks, name) => {
      callbacks.data( 'name', name );
      callbacks.done();
    }
  });
};

exports.init = init;

// Register the handler to be called whenever the name changes.
var onName = function( handler ) {
  flux.registerListener( handler, 'Name' );
};

exports.onName = onName;

// Queue the action to change the name.
var setName = function( name ) {
  flux.queue( 'set-name', name );
};

exports.setName = setName;

