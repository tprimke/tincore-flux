var flux  = require('tc-flux'),
    model = require('./flux-model'),
    React = require('react');

var NameInput = React.createClass({

  getInitialState: function() {
    return {
      value: ''
    };
  },

  render: function() {
    return (
      <input type="text" value={this.state.value} onChange={this.onName} />
    );
  },

  onName: function(evt) {
    let name = evt.target.value;
    this.setState({value: name});
    model.setName(name);
  }

});

var Message = React.createClass({

  getInitialState: function() {
    return {
      name: flux.getStore('Name')
    };
  },

  componentWillMount: function() {
    model.onName( this.onName );
  },

  render: function() {
    let name = this.state.name;
    if ( name === '' )
      return <p>Please enter your name.</p>;
    else
      return (
        <h1>Hello, {name}!</h1>
      );
  },

  onName: function(name) {
    this.setState({name});
  }

});


var App = React.createClass({

  render: function() {
    return (
      <div>
        <NameInput />
        <Message />
      </div>
    );
  }

});

model.init();
React.render( <App />, document.getElementById('app') );
