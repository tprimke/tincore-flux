var mocha  = require('mocha'),
    should = require('should'),
    flux   = require('../build/tmp/flux-model');

describe( 'The Flux model', function() {
  
  beforeEach( function() {
    // initialize the tested module
    flux.init();
  });

  it('the registered callbacks are called', function(done) {
    
    // register a callback
    flux.onName( (name) => {
      name.should.eql('ok');
      done();
    });

    // queue the change name action
    flux.setName( 'ok' );
    
  });

}); // The Flux model

