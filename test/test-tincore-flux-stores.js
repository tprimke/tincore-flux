var mocha = require('mocha'),
    should = require('should'),
    _ = require('lodash'),

    flux = require( '../build/tc-flux' );

describe( 'tincore-flux', function() {

  beforeEach( function() {
    flux.clear();
  });
  
  
  // The stores part.
  describe( 'Stores', function() {
    
    it('create', function() {
      
      // create a new store
      flux.createStore({
        name: 'TestCreate',

        init: {
          value: 'test'
        }
      });

      // get the store and verify the initial value
      var store = flux.getStore('TestCreate');
      store.should.have.property('value', 'test');
    });

    it('cannot modify store using getStore', function() {

      flux.createStore({
        name: 'TestNoModify',

        init: {
          value: 'testNoModify'
        }
      });

      // get the store and change the value
      var store = flux.getStore('TestNoModify');
      store.value = 'Ooops!';

      // get the store once again; the value should be unchanged
      store = flux.getStore('TestNoModify');
      store.should.have.property('value', 'testNoModify');
    });

    it( 'onData handler is called', function(done) {

      // create two stores, with different data signatures

      flux.createStore({
        name: 'TestOnData1',

        data: 'on-data-1',

        onData: function() {
          done();
        }
      });

      flux.createStore({
        name: 'TestOnData2',

        data: 'on-data-2',

        onData: function() {
          done('Wrong handler called');
        }
      });

      // create and queue an action, which introduced a new data of
      // one of the used signatures
      var action = flux.createAction({
        onRun: function(callbacks) {
          callbacks.data( 'on-data-1' );
        }
      });

      flux.queue( action );
    }); // onData handler is called
    
    it( 'onData handler called for arrays', function(done) {
      
      // the same test as the previous one, but arrays are used for
      // data signatures
      flux.createStore({
        name: 'TestOnData11',

        data: ['on-data-11'],

        onData: function() {
          done();
        }
      });

      flux.createStore({
        name: 'TestOnData22',

        data: ['on-data-22'],

        onData: function() {
          done('Wrong handler called');
        }
      });

      var action = flux.createAction({
        onRun: function(callbacks) {
          callbacks.data( 'on-data-11' );
        }
      });

      flux.queue( action );
    }); // onData handler called for arrays

    it( 'updated handler is called and can change the store data', function(done) {

      // create a new store
      // the `onData` handler should change the store's value and call
      // the `updated` callback
      flux.createStore({
        name: 'TestUpdated',

        data: 'test-updated',

        init: 0,

        onData: function(callbacks, data) {
          callbacks.setValue(1);
          callbacks.updated();
        }
      });

      // create an action used to send a new data to the store
      var action = flux.createAction({
        onRun: function(callbacks) {
          callbacks.data( 'test-updated' );
        }
      });

      // the handler should be called with the new store's data
      var handler = function( store_data ) {
        store_data.should.eql(1);
        done();
      };

      // register the handler
      flux.registerListener( handler, 'TestUpdated' );

      // queue the action
      flux.queue( action );
    }); // updated handler is called

    it( 'notUpdated does not call the handler', function(done) {
      
      // create a new store
      // the `onData` handler should call the `notUpdated` callback
      flux.createStore({
        name: 'TestNotUpdated',

        data: 'test-not-updated',

        onData: function(callbacks, data) {
          callbacks.notUpdated();
        }
      });

      // create an action used to send new data to the store
      var action = flux.createAction({
        onRun: function(callbacks) {
          callbacks.data( 'test-not-updated' );
        }
      });

      // create and register a handler, which shouldn't have been
      // called at all
      var handler = function() {
        done( "Handler shouldn't have been called." );
      };
      flux.registerListener( handler, 'TestNotUpdated' );

      // queue the action; nothing should happen
      flux.queue( action );

      // after a short delay, the test will be passed
      setTimeout( function() {
        done();
      }, 100 );
    }); // notUpdated does not calls the handler

    it('passing data to the onData handler', function(done) {

      // create a store
      // the `onData` handler should verify the passed data
      flux.createStore({
        name: 'TestData',

        data: 'test-data',

        onData: function(callbacks, data) {
          data.should.eql('data');
          done();
        }
      });

      // create and queue an action used to pass the data to the store
      var action = flux.createAction({
        onRun: function(callbacks) {
          callbacks.data( 'test-data', 'data' );
        }
      });

      flux.queue(action);
    });
    
    it( 'store without data signatures should receive all callbacks', function(done) {

      // an array to accumulate `onData` handlers calls
      var arr = [];

      // create the first store, with no data signature
      flux.createStore({
        name: 'TestSignatures1',

        onData: function(callbacks) {
          arr.push(0);
          callbacks.notUpdated();
        }
      });

      // create the second store, with a data signature
      flux.createStore({
        name: 'TestSignatures2',
        
        data: 'test-signatures',
        
        onData: function(callbacks) {
          arr.push(1);
          callbacks.notUpdated();
        }
      });

      // create the action to send a data signature
      flux.createAction({
        name: 'TestSignatures',
        onRun: function(callbacks, sig) {
          callbacks.data(sig);
          callbacks.done();
        }
      });

      // send data with signature to be received by both stores
      flux.queue( 'TestSignatures', 'test-signatures' );

      // send data with signature to be received by the first store only
      flux.queue( 'TestSignatures', 'yada' );

      // as the result, three values should be pushed to the array
      setTimeout( function() {
        arr.length.should.eql(3);
        done();
      }, 100 );
    }); // store without data signatures should receive all callbacks

    it( 'data should be sent to all stores interested in particular data signature', function(done) {
      
      // an array to accumulate `onData` handlers results
      var arr = [];

      // create the stores interested in the same data signature
      flux.createStore({
        name: 'TestMultiSignatures1',
        data: 'test-multisignature',
        onData: function(callbacks) {
          arr.push(0);
          callbacks.notUpdated();
        }
      });
      flux.createStore({
        name: 'TestMultiSignatures2',
        data: 'test-multisignature',
        onData: function(callbacks) {
          arr.push(1);
          callbacks.notUpdated();
        }
      });

      // create the action to send the data signature
      flux.createAction({
        name: 'TestSignatures',
        onRun: function(callbacks) {
          callbacks.data('test-multisignature');
          callbacks.done();
        }
      });

      // queue the action
      flux.queue( 'TestSignatures' );

      // after the action is finished, the array should contain two values
      setTimeout( function() {
        arr.length.should.eql(2);
        done();
      }, 100 );
      
    }); // data should be sent to all stores interested in particular data signature

    it( "merge store's object", function(done) {
      
      // create a store
      flux.createStore({
        name: 'TestMerge',
        init: {
          test: 'Ooops'
        },
        data: 'not-important',
        onData: function( callbacks, data ) {
          callbacks.mergeValue( data );
          callbacks.updated();
        }
      });

      // create the action to send data to the store
      flux.createAction({
        name: 'TestSignatures',
        onRun: function( callbacks ) {
          callbacks.data( 'not-important', { test: 'ok' } );
          callbacks.done();
        }
      });

      // queue the action
      flux.queue( 'TestSignatures' );

      // after the action is finished, the store's object should be modified
      setTimeout( function() {
        var store = flux.getStore('TestMerge');
        store.should.have.property('test', 'ok');
        done();
      }, 100 );

    }); // "merge store's object"
    
    it( 'data signatures in the object passed to the onData handler', function(done) {
      
      // create the store
      flux.createStore({
        name: 'TestSignatureObject',
        data: 'test-sig',
        onData: function( callbacks ) {
          callbacks.should.have.property('signature', 'test-sig');
          callbacks.notUpdated();
        }
      });

      // create the action to send data to the store
      flux.createAction({
        name: 'TestSignatureObject',
        onRun: function( callbacks ) {
          callbacks.data( 'test-sig' );
          callbacks.done();
        }
      });
      
      // queue the action
      flux.queue( 'TestSignatureObject' );

      // after the action is finished, the test should be passed
      setTimeout( function() {
        done();
      }, 100 );
    }); // data signatures in the object passed to the onData handler
    

  }); // Stores
  
}); // tincore-flux

