var gulp       = require('gulp'),
    to5        = require('gulp-babel'),
    del        = require('del'),
    rename     = require('gulp-rename'),
    mocha      = require('gulp-mocha'),
    browserify = require('gulp-browserify2');

// Build jsni

gulp.task( 'clean', function(cb) {
  del([
    'build/*'
  ], cb );
});

gulp.task( 'build-es6', ['clean'], function() {
  return gulp.src('src/js/**/*.es6')
             .pipe(to5())
             .pipe(rename( function(path) {
               path.extname = '.js';
             }))
             .pipe( gulp.dest('build/tmp') );
});

gulp.task( 'build-app', ['build-es6'], function() {
  return gulp.src('build/tmp/app.js')
             .pipe( browserify({
               fileName: 'app.js',
               options: {
                 debug: false
               }
             }))
             .pipe( gulp.dest('build') );
});

gulp.task( 'build-html', ['clean'], function() {
  return gulp.src('src/html/**/*.html')
             .pipe( gulp.dest('build') );
});

gulp.task( 'build', ['build-html', 'build-app'] );

// Test

gulp.task( 'clean-test', function(cb) {
  del([
    'test/*.js'
  ], cb);
});

gulp.task( 'build-test', ['clean-test'], function() {
  return gulp.src('test/test-*.es6')
             .pipe( to5() )
             .pipe( rename( function(path) {
               path.extname = '.js';
             }))
             .pipe( gulp.dest('test') );
});

gulp.task( 'test', ['build-es6', 'build-test'], function() {
  return gulp.src(['test/test-*.js'], {read: false})
             .pipe(mocha({
               reporter: 'spec'
             }));
});

