var mocha = require('mocha'),
    should = require('should'),
    _ = require('lodash'),

    flux = require( '../build/tc-flux' );

describe( 'tincore-flux', function() {
  
  // The actions part
  describe( 'Actions', function() {
    
    it( 'only actions can be queued', function() {

      // anything, that is not an action, when queued, should throw
      // this test is not perfect, but it's better than nothing at all
      
      (function() { flux.queue(1); }).should.throw();
      (function() { flux.queue('q'); }).should.throw();
      (function() { flux.queue(true); }).should.throw();
      (function() { flux.queue({}); }).should.throw();
      (function() { flux.queue([]); }).should.throw();
      var func = function() {};
      (function() { flux.queue(func); }).should.throw();

    });

    it( 'callback', function(done) {

      // create a simple action, with the `onRun` handler
      var action = flux.createAction({
        name: 'some-action',
        onRun: function(callbacks) {
          done();
        }
      });

      // queue the action; the `onRun` handler should be called
      flux.queue( action );
    }); // callback

    it( 'start handler', function(done) {
      
      // a new, empty array
      var arr = [];

      // create a new action
      // the `onStart` handler should push a value to the array
      // the `onData` handler should verify the value
      var action = flux.createAction({
        name: 'some-action',
        onRun: function(callbacks) {
          arr.length.should.eql(1);
          arr[0].should.eql(0);
          done();
        },

        onStart: function(handlers) {
          arr.push(0);
        }
      });

      // queue the action
      flux.queue( action );
    }); // start handler

    it( 'end handler', function(done) {

      // create a new action
      // the `onEnd` handler should be called
      var action = flux.createAction({
        name: 'some-action',
        onRun: function(callbacks) {
          callbacks.done();
        },

        onEnd: function(handlers) {
          done();
        }
      });

      flux.queue( action );
    }); // end handler

    it( 'action data', function(done) {

      // verify, whether the data passed to the queue function is
      // passed to the `onRun` handler
      var action = flux.createAction({
        name: 'action-data',

        onRun: function( callbacks, data ) {
          data.should.eql('test');
          done();
        }
      });

      flux.queue( action, 'test' );
    }); // case

    it( 'queue action by name', function(done) {
      
      flux.createAction({
        name: 'test-action-name',

        onRun: function() {
          done();
        }
      });

      flux.queue( 'test-action-name' );

    }); // queue action by name

    it( 'overriding actions names', function(done) {
      
      flux.createAction({
        name: 'test-override-1',
        onRun: function() {
          done('Ooops');
        }
      });

      flux.createAction({
        name: 'test-override-1',
        onRun: function() {
          done();
        }
      });

      flux.queue( 'test-override-1' );
    }); // overriding actions names
    
  }); // Actions
  
}); // tincore-flux

