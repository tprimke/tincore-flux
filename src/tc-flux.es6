var _ = require('./lodash.custom.js');

// ------------------------------------------------------------------
// ---------- The dispatcher
// ------------------------------------------------------------------

// The dispatcher is a "store" for information about stores and
// actions.
//
// The information is kept in the form of arrays, for it can be easily
// searched using the lodash functions.
var dispatcher = {
  stores: [],
  actions: []
};

// Adds a new store to the dispatcher.
// This method should be called exactly once for each created store.
//
// The specification object (the argument) describes the store:
//   name - the name of the store (it should be unique for obvious
//   reasons)
//   data - the data signatures the store being added is interested in
//
// This method merely adds a new "data object" to the array mentioned
// above.
//
// One additional field is added to the specification: the listeners
// array (an empty one).
var addStore = function( spec ) {
  // no signatures info requires special handling
  if ( ! spec.hasOwnProperty('data') )
    spec.data = null;

  // the data signatures, the store is interested in
  // it should be an array
  var the_data;
  if ( ! _.isArray( spec.data ) )
    the_data = [ spec.data ];
  else
    the_data = spec.data;

  dispatcher.stores.push({
    name: spec.name,
    data: the_data,
    listeners: []
  });
};

// Adds a new action to the store.
// This function should be called exactly once for each created
// action.
//
// The specification object (the argument) describes the action:
//   - name: the name of the action,
//   - action: the action object
var addAction = function( spec ) {
  // remove the old action (if it exists)
  _.remove( dispatcher.actions, { name: spec.name } );

  // add the new action
  dispatcher.actions.push({
    name: spec.name,
    action: spec.action
  });
};

// Adds a new listener for the specified store.
// This function should be called exactly once for each listener.
//
// The specification object (the argument) describes the listener:
//   - store: the name of the store, the listener is interested in
//   - listener: the function (a handler) to be called, whenever the
//   store updates
var addListener = function( spec ) {
  // find the store (info)
  var the_store = _.findWhere( dispatcher.stores, { name: spec.store } );
  if ( the_store === undefined )
    return;

  // add the listener to the store (info)
  the_store.listeners.push( spec.listener );
};

// Sends the given data to the given store, by calling the store's
// onData handler.
//
// This function is used internally by the onData one.
var onSingleStore = function( disp_store, data, signature ) {
  // get the store object
  var the_store = stores[ disp_store.name ];

  // call the store's onData handler
  the_store.onData({

    // sets the value of the store
    setValue: (val) => {
      the_store.value = val;
    },

    // merge the value of the store
    mergeValue: (val) => {
      _.merge( the_store.value, val );
    },

    // the store value has been updated;
    // pass the new store data to all its listeners
    updated: () => {
      let store_data = _.clone( the_store.value, true );
      _.forEach( disp_store.listeners, (listener) => { listener( store_data ); } );
    },

    // the store value hasn't been changed
    // nothing is done right now
    notUpdated: () => {
    },

    // the signature
    signature

  }, data);
};

// Manages the `data` handler passed to action's `onRun` handler.
//
// Arguments:
//   - signature: the data signature
//   - data: the data itself
//
// Right now, only one store is looked for for the given signature.
var onData = function( signature, data ) {

  // find all the stores without any data signatures
  var disp_stores = _.filter( dispatcher.stores, (store) => (store.data.length === 1) && (store.data[0] === null) );
  if ( disp_stores.length > 0 )
    _.each( disp_stores, (disp_store) => onSingleStore(disp_store, data) );

  // find all the stores interested in the given signature
  var disp_stores = _.filter( dispatcher.stores, (store) => { return _.indexOf( store.data, signature ) !== -1; } );
  
  if ( disp_stores.length === 0 )
    return;

  _.each( disp_stores, (disp_store) => onSingleStore(disp_store, data, signature) );

};

// ------------------------------------------------------------------
// ---------- Actions
// ------------------------------------------------------------------

// Creates a new action.
// This function is intended for the end-user.
//
// The specification object (the argument) describes the action to be
// created:
//   - name: the name of the action (the name can be used while queing
//   actions)
//   - onRun: the handler, which should be called whenever the action
//   is executed,
//   - onStart: the handler, which should be called before the action
//   is executed
//   - onEnd: the handler, which should be called after the action is
//   finished
//
// Before the action is executed, the onStart handler is called.
//
// The onRun handler is passed two arguments: callbacks and data.
// The callbacks object has two handlers:
//   - done: it should be called, when the action is done (finished)
//   - data: it should be called, when new data have been received as
//   a result of the action
//
// After the action is finished, the onEnd handler is called.
var createAction = function( spec ) {

  // create the action object
  var action = (function() {

    var callbacks = {
      // the `done` callback merely calls the `onEnd` handler, if
      // specified
      done: () => {
        if ( spec.hasOwnProperty('onEnd') )
          spec.onEnd();
      },

      // this callback is more complex, so it was moved to a sepatare
      // function
      data: onData
    };

    // the action object interface
    return {
      // it is used to mark the object as an action
      // yeah, I know, it's not a good mark; perhaps it should be
      // solved in some better way (a class?)
      _action: true,

      // runs the action
      run: (data) => {
        // call the `onStart` handler, if provided
        if ( spec.hasOwnProperty('onStart') ) {
          spec.onStart();
        }

        // call the `onRun` handler - if provided
        if ( spec.hasOwnProperty('onRun') ) {
          spec.onRun(callbacks, data);
        }
      }
    };
  })();

  // adds the action info to the dispatcher
  addAction({
    name: spec.name,
    action: action
  });

  // return the action object
  // perhaps it should be removed in the future?
  return action;
};

exports.createAction = createAction;

// The function used to check, whether the given argument is an action
// object or not.
var isAction = function( action ) {
  // when the action is a string, it can be an action's name
  if ( _.isString( action ) ) {
    let the_action = _.find( dispatcher.actions, { name: action } );
    return the_action !== undefined;
  }

  // an action should be an object with a property "_action"

  if ( ! _.isObject(action) )
    return false;

  if ( ! action.hasOwnProperty('_action') )
    return false;

  return true;
};

// Queues the given action, to be called with the given data.
//
// Arguments:
//   - action: either the action's name, or the action's object
//   - data: optional data that should be passed to the action's
//   `onRun` handler
//
// In general, this function should be used to *queue* actions.
//
// The attention, please: *queue*, not execute nor run.
//
// Right now, though, the actions are run immediatelly. It may change
// in the future, when dispatcher becomes more complex (and when some
// actions will be blocked, for example). Then, some more
// sophisticated scheduling algorithms may be used.
var queue = function( action, data ) {
  if ( ! isAction( action ) )
    if ( _.isString( action ) )
      throw Error( `Only actions can be queued. (Unknown action ${action})` );
    else
      throw Error( 'Only actions can be queued.' );

  if ( _.isString( action ) )
    action = (_.find( dispatcher.actions, { name: action } )).action;

  action.run(data);
};

exports.queue = queue;

// ------------------------------------------------------------------
// ---------- Stores
// ------------------------------------------------------------------

// The stores.
// I think it should be moved to the dispatcher object.
var stores = {};

// The dummy `onData` handler, used when a store's creator forgets his
// own one.
var dummyOnData = function() {
};

// Creates a new store.
//
// The specification object (the argument) describes the store:
//   - name: the name of the store
//   - init: optional, initial value of the store
//   - data: the data signatures the store is interested in
//   - onData: the handler to be called whenever the data of the
//   interesting signature is obtained as a result of any action
//
// The data signatures may be given in a form of a string (a single
// signature), or an array of strings (many signatures).
//
// The `onData` handler is passed two arguments: `callbacks` and
// `data`.
//
// The `callbacks` objects contains handlers, which should be used for
// the following purposes:
//   - updating the store: `callbacks.setValue( new_store_value )`
//                         `callbacks.mergeValue( value_to_merge )`
//   - indicating that the store has been updated:
//   `callbacks.updated()`
//   - indicating that the store hasn't been updated:
//   `callbacks.notUpdated()`
//   - and the data signature: `callbacks.signature`
//
// The `data` is the data that came with the signature.
var createStore = function( spec ) {
  // save a new store object
  stores[ spec.name ] = {
    value: spec.init,
    onData: spec.onData || dummyOnData
  };

  // add the store info to the dispatcher
  addStore( spec );
};

exports.createStore = createStore;

// Returns the store's value for the given store name.
var getStore = function( name ) {
  var store = stores[name];
  if ( store !== undefined )
    return _.clone( store.value, true );
};

exports.getStore = getStore;

// Register a new listener for the given store name.
//
// When a listener is called, it is passed the new value of the store.
var registerListener = function( listener, store_name ) {
  addListener({
    store: store_name,
    listener: listener
  });
};

exports.registerListener = registerListener;

// Resets the module.
//
// After calling this method, no stores nor actions are available.
//
// It can be used for testing.
var clear = function() {
  stores = {};
  dispatcher = {
    stores:  [],
    actions: []
  };
};

exports.clear = clear;
