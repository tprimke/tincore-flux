var mocha = require('mocha'),
    should = require('should'),
    _ = require('lodash'),

    flux = require( '../build/tc-flux' );

describe( 'tincore-flux', function() {

  describe( 'Clear', function() {
    
    it('return to the initial state', function() {
      flux.createAction({
        name: 'test',
        onRun: function(callbacks) {
          callbacks.done();
        }
      });
      flux.clear();
      (function() { flux.queue('test');}).should.throw();
    });
  }); // Clear
  
}); // tincore-flux

